package com.finacialhospitalassignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import com.finacialhospitalassignment.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initView();
    }

    private void initView() {
        mBinding.rlInfo.setOnClickListener(this);
        mBinding.rlKnowYouBtr.setOnClickListener(this);
        mBinding.rlKnowYourRisk.setOnClickListener(this);
        mBinding.rlYourFamily.setOnClickListener(this);
        mBinding.switchEquity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mBinding.tvFixed.setTextColor(getResources().getColor(R.color.lightGray));
                    mBinding.tvVariable.setTextColor(getResources().getColor(R.color.black));
                } else {
                    mBinding.tvVariable.setTextColor(getResources().getColor(R.color.lightGray));
                    mBinding.tvFixed.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rlInfo:
                mBinding.rlInfo.setBackgroundColor(getResources().getColor(R.color.lightRed));
                mBinding.rlKnowYouBtr.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlKnowYourRisk.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlYourFamily.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));

                break;
            case R.id.rlKnowYouBtr:
                mBinding.rlInfo.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlKnowYouBtr.setBackgroundColor(getResources().getColor(R.color.lightRed));
                mBinding.rlKnowYourRisk.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlYourFamily.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));

                break;
            case R.id.rlKnowYourRisk:
                mBinding.rlInfo.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlKnowYouBtr.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlKnowYourRisk.setBackgroundColor(getResources().getColor(R.color.lightRed));
                mBinding.rlYourFamily.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));

                break;
            case R.id.rlYourFamily:
                mBinding.rlInfo.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlKnowYouBtr.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlKnowYourRisk.setBackgroundColor(getResources().getColor(R.color.extremeLightRed));
                mBinding.rlYourFamily.setBackgroundColor(getResources().getColor(R.color.lightRed));

                break;
        }
    }
}